const path = require('node:path');

const getRoot = () => path.join(__dirname, '../../../../');

module.exports = getRoot();