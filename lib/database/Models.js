'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { Sequelize } = require('sequelize');
const basename = path.basename(__filename);
const config = require('../../../../config/config');
const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
    logging: config.logging
});

const modelRoot = path.join(__dirname, '../../../../models')

fs
    .readdirSync(path.join(modelRoot))
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = require(path.join(modelRoot, file))(sequelize, Sequelize.DataTypes);
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.Op = Sequelize.Op;

module.exports = db;
