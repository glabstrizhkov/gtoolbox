const { NotFoundError, ConflictError } = require('../../errors');

const checkByObj = {
  is_exist: (Model, key, stateName, required = true) => async (req, res, next) => {
    const value = req.params[key] || req.query[key] || req.body[key];

    if (required && !value) return next(new NotFoundError({ message: `${key} IS NOT FOUND` }));

    if(value){
      let where = {};
      where[key] = value;

      const object = await Model.findOne({ where });
      req.state = {};
      if(object) req.state[stateName] = object;
      return next();
    }
  }
}

module.exports = { checkByObj };
