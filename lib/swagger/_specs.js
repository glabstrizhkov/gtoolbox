const swaggerJsdoc = require('swagger-jsdoc');

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'a1 api',
            version: '1.0.0',
        },
    },
    apis: ['./docs/*.yml', './docs/*/*.yml'], // files containing annotations as above
};

const _specs = swaggerJsdoc(options);

module.exports = { _specs };
