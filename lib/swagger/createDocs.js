const swaggerUi = require('swagger-ui-express');
const { _specs } = require('./_specs');

const createDocs = (server, url) =>
    server.use(url, swaggerUi.serve, swaggerUi.setup(_specs));


module.exports = { createDocs };