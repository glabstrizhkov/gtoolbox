const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');
const { createDocs } = require('../swagger');

const createServer = (docsUrl = '' || null) => {
    const app = new express();

    app.use(cors());
    app.use(express.json())
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ urlencoded: true }));
    app.use(express.text({ type: 'application/json' }));

    if(docsUrl)
        createDocs(app, docsUrl);

    return app;
}

module.exports = { createServer };