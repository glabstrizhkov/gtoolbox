const path = require('node:path');
const { getRoot } = require('../helpers');


// const demo = [
//     {
//         url: '/hallo/world',
//         method: 'get',
//         params: [ 'name', 'age' ],
//         controller: './controllers/hallo'
//     }
// ]


const State = {
    GET: {},
    POST: {},
    PATCH: {},
    DELETE: {},

    init(saveJSON = {}){
        for (let i = 0; i < saveJSON.length; i++) {
            const { url, method, params, controller } = saveJSON[i];
            if(!url || !params || !controller){
                console.error(`INVALID ROUTER MODEL | url, params & controller are required for each item | check item number ${i}`);
                process.exit(1);
            }

            const _controller = require(path.join(getRoot, controller));

            if(method === 'get' || 'GET')
                this.GET[url] = { params, controller: _controller };

            if(method === 'post' || 'POST')
                this.POST[url] = { params, controller: _controller };

            if(method === 'patch' || 'PATCH')
                this.PATCH[url] = { params, controller: _controller };

            if(method === 'delete' || 'DELETE')
                this.DELETE[url] = { params, controller: _controller };
        }
    },

    getRoute: (url, method) => {
        if(method === 'get' || 'GET')
            return this.GET[url];

        if(method === 'post' || 'POST')
            return this.POST[url];

        if(method === 'patch' || 'PATCH')
            return this.PATCH[url];

        if(method === 'delete' || 'DELETE')
            return this.DELETE[url];
    }
}

module.exports = State;