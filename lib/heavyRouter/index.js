const { init } = require('./State');
const { router } = require('./fork');

module.exports = { init, router };