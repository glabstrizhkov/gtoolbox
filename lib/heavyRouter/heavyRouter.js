const state = require('./State');


const heavyRouter = (req, res) => {
    const { body, method, query, baseUrl, path } = req;
    let url = baseUrl + path;

    const { params, controller } = state.getRoute(url);
    let paramList = {};

    if(params.length > 0){
        const parts = url.split('/');
        let paramBackCounter = params.length - 1;

        for (let i = parts.length - 1; i >= 0; i--) {
            paramList[params[paramBackCounter]] = parts[i];

            if(paramBackCounter === 0) break;
            paramBackCounter--;
        }

        for (let i = 0; i < parts.length; i++) {
            url += `/${parts[i]}`;
        }
    }

    const inData = { url, body, query, params: paramList };
    controller(inData, res);
}

module.exports = heavyRouter;