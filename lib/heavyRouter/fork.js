const { Router } = require('express')
const heavyRouter = require('./heavyRouter');


const router = Router();
router.get('/*', heavyRouter);
router.post('/*', heavyRouter);
router.patch('/*', heavyRouter);
router.delete('/*', heavyRouter);

module.exports = { router };