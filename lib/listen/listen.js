const { dbAuthenticate, Models } = require('../database');


const listen = async (server, PORT, useDB = false) => {
    if(useDB) await dbAuthenticate(Models.sequelize);
    await server.listen(PORT, () => console.info(`Server listens on port ${ PORT }`));
}

module.exports = { listen };