const errors = require('./errors');
const database = require('./database');
const base = require('./base');
const server = require('./server');
const swagger = require('./swagger');
const listen = require('./listen');
const templates = require('./templates');
const helpers = require('./helpers');
// const heavyRouter = require('./heavyRouter');

module.exports = {
    errors,
    database,
    ...base,
    ...server,
    ...swagger,
    listen,
    ...templates,
    helpers,
    // heavyRouter
};
