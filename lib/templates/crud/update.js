
const update = (Model, where = {}, data = {}) =>
    Model.update(data, { where });


module.exports = { update }
