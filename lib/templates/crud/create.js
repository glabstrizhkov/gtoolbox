
const create = (Model, data) =>
    Model.create(data);

module.exports = { create };
