
const removeOne =  (Model, where = {}) =>
    Model.destroy({ where, force: true });


module.exports = { removeOne };