
const findOne = (Model, where = {}, attributes = [] || undefined) =>
    Model.findOne({ where, attributes });

const findAll = (Model, attributes = [] || undefined) =>
    Model.findAll({ attributes });


const filter = (Model, where = {}, attributes = [] || undefined) =>
    Model.findAll({ where, attributes });


module.exports = { findOne, findAll, filter }